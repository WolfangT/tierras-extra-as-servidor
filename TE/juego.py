"""Modulo de Juego

Define los conceptos de alto nivel necesarios para simular un juego.
"""

from logging import getLogger
from uuid import uuid4

from TE.faltas import LimiteReacurso, NoSuficientesRecursos, \
    NoSuficienteNivel


REG = getLogger(__name__)


class Juego(object):
    """Clase principal del Juego, interfaz por donde controlar todos los
componentes"""

    _turno = 0

    def __init__(self, mapa, personajes):
        self.mapa = mapa
        self.lista_personajes = ListaPersonajes(personajes)

    def buscar_personaje(self, codigo):
        """Regre un Personaje con un guid especifico"""
        for personaje in self.personajes:
            if hash(personaje) == hash(codigo):
                return personaje
        return None

    @property
    def personajes(self):
        """Regresa la lista con todos los personajes"""
        return self.lista_personajes.personajes

    @property
    def personajes_activos(self):
        """Regresa un a lista con todos los personajes activos"""
        return self.lista_personajes.personajes_activos

    @property
    def numero_turno(self):
        """Regresa en numero de turno actual"""
        return self._turno

    @property
    def numero_periodo(self):
        """Regresa el numero del periodo actual"""
        return self.lista_personajes.nivel + 1


class Personaje(object):
    """Clase base para personajes"""

    activo = True

    def __init__(self, nombre, resistencia=0, energia=0, agilidad=0,
                 efecto=None, arte=None, carta=None):
        self.nombre = nombre
        self._uuid = int(uuid4())
        self._resistencia = resistencia
        self._energia = energia
        self._agilidad = agilidad
        self.efecto = efecto
        self.recursos = []
        self.habilidades = []
        self.arte = arte
        self.carta = carta

    def acoplar(self, carta):#TODO
        """Acopla una carta de recurso al personaje"""
        mapeo_max = {
            'Resistencia': self._resistencia,
            'Energia': self._energia,
            'Agilidad': self._agilidad}
        if carta.tipo == 'Recurso':
            if carta.nivel > self.carta.nivel:
                raise NoSuficienteNivel(self.carta.nivel, carta.nivel)
            maximo = mapeo_max[carta.subtipo]
            actual = self.obtener_atributo(carta.subtipo)
            if (actual + carta.valor) <= maximo:
                self.recursos.append(carta)
            else:
                raise LimiteReacurso(carta.subtipo)
        elif carta.tipo == 'Habilidad':
            self.habilidades.append(carta)

    def desactivar_recursos(self, tipo, cantidad=1):
        cantidad = abs(cantidad)
        valor = 0
        if cantidad == 0:
            return
        for carta in self.recursos:
            if carta.subtipo == tipo and carta.activa:
                if valor + carta.valor > cantidad:
                    break
                carta.activa = False
                valor += carta.valor
            if valor == cantidad:
                break
        else:
            raise NoSuficientesRecursos(cantidad, tipo)

    def destruir_recursos(self, tipo, cantidad=1):
        cantidad = abs(cantidad)
        valor = 0
        cartas = []
        for carta in self.recursos:
            if carta.subtipo == tipo:
                if valor + carta.valor > cantidad:
                    break
                cartas.append(carta)
                valor += carta.valor
            if valor == cantidad:
                break
        else:
            raise NoSuficientesRecursos(cantidad, tipo)
        for carta in cartas:
            del self.recursos[self.recursos.index(carta)]
        return cartas

    def obtener_atributo(self, atributo):
        if atributo == 'Resistencia':
            return self.resistencia
        if atributo == 'Energia':
            return self.energia
        if atributo == 'Agilidad':
            return self.agilidad

    def obtener_max_atributo(self, atributo):
        if atributo == 'Resistencia':
            return self._resistencia
        if atributo == 'Energia':
            return self._energia
        if atributo == 'Agilidad':
            return self._agilidad

    @property
    def cartas(self):
        return self.recursos + self.habilidades + [self.carta]

    @property
    def resistencia(self):
        valor = 0
        for carta in self.recursos:
            if carta.subtipo == 'Resistencia' and carta.activa:
                valor += carta.valor
        return valor

    @property
    def energia(self):
        valor = 0
        for carta in self.recursos:
            if carta.subtipo == 'Energia' and carta.activa:
                valor += carta.valor
        return valor

    @property
    def agilidad(self):
        valor = 0
        for carta in self.recursos:
            if carta.subtipo == 'Agilidad' and carta.activa:
                valor += carta.valor
        return valor

    @property
    def numero_max_recursos(self):
        return self._resistencia + self._energia + self._agilidad

    def __str__(self):
        return '{}(R:{}, E:{}, A:{})'.format(self.nombre, self.resistencia,
                                             self.energia, self.agilidad)

    def __hash__(self):
        return self._uuid

    def __eq__(self, objeto):
        if type(objeto) is type(self):
            return bool(hash(objeto) == hash(self))
        else:
            return NotImplemented

    def __ne__(self, objeto):
        return not self.__eq__(objeto)

    def __bool__(self):
        return self.activo


class ListaPersonajes(object):
    """Contenedor que mantiene el orden de jugada"""

    nivel = 0

    def __init__(self, personajes):
        self.personajes = personajes

    def __iter__(self):
        """Reorganiza a los personajes por nivel de energia"""
        self.nivel = self._calcular_orden()[0]
        return self

    def __next__(self):
        if self.nivel < 0:
            raise StopIteration
        else:
            self.nivel, periodos = self._calcular_orden()
            personajes = periodos[self.nivel]
            self.nivel -= 1
            return personajes

    def _calcular_orden(self):
        personajes = [
            (personaje.energia, personaje)
            for personaje in self.personajes
            if personaje.activo]
        try:
            maximo = max([personaje[0] for personaje in personajes])
        except ValueError:
            maximo = 0
        periodos = {}
        for periodo in range(maximo, -1, -1):
            periodos[periodo] = [
                personaje[1]
                for personaje in personajes
                if personaje[0] >= periodo]
        return maximo, periodos

    @property
    def personajes_activos(self):
        periodos = self._calcular_orden()[1]
        try:
            return periodos[self.nivel + 1]
        except KeyError:
            return []
