"""Modulo de Inteligencias artificiales"""

from logging import getLogger


REG = getLogger(__name__)


class IA():
    """Clase base para Inteligencias Artificiales"""

    def __call__(self, juego):
        pass


class Humano(IA):
    """Objeto que representa a un jugador humano"""

    def __call__(self, personaje, juego):
        return eval(input('Jugada de {}: '.format(personaje.nombre)))
