"""Constantes globales del sistema"""

from os import path
from glob import glob

# Cartas
LICENCIA_CARTA = "®Tierras extrañas: Escaramusas, por \
Wolfang Torres, Todos los derechos reservados"
LISTA_CARTAS = glob(path.join(path.abspath(__file__), 'cartas', '*.xml'))

# Reglas
NADA = bin(0)
PERIODO = bin(1)
TURNO = bin(2)

# Mapa - Direcciones
N = 'n'
S = 's'
E = 'e'
O = 'o'
