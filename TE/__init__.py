"""Sistema de fondo de Tierras Extrañas: Batallas y Escaramusas.

Contiene el funcionamiento del juego, el cual es utilisado por tanto el
servidor y cliente para poder funcionar"""

__all__ = [
    'cartas',
    'faltas',
    'globales',
    'ia',
    'juego',
    'mapa',
    'reglas',
    'habilidades',
]
