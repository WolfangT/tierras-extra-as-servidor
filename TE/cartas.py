"""Modulo que define el concepto de carta.

Define todas las caracteristicas de una carta, ademas inlucye funciones
para crearlas a base de un archivo XML y crea una base de datos locasl
de Cartas
"""
from copy import copy
from logging import getLogger
from xml.etree.ElementTree import parse

from TE.globales import LICENCIA_CARTA, LISTA_CARTAS


REG = getLogger(__name__)


class Metadatos():
    """Componente de Carta que representa la informacion de identificacion"""

    licencia = LICENCIA_CARTA

    def __init__(self, temporada=0, expancion=0, serial=0, vercion=0,
                 nombre_temporada='', nombre_expancion=''):
        self.temporada = int(temporada)
        self.expancion = int(expancion)
        self.serial = serial
        self.vercion = vercion
        self.nombre_temporada = nombre_temporada
        self.nombre_expancion = nombre_expancion

    def __hash__(self):
        return int('1{0:02d}{1:02d}{2:02d}{3:02d}'.format(
            int(self.temporada) if self.temporada else 0,
            int(self.expancion) if self.expancion else 0,
            int(self.serial) if self.serial else 0,
            int(self.vercion) if self.vercion else 0))


class Imagen():
    """Objeto que representa la imagen de una Carta y sus Metadatos"""

    _imagen = None

    def __init__(self, archivo='', historia='', artista=''):
        self.archivo = archivo if archivo else 'Fondo.png'
        self.historia = historia
        self.artista = artista


class Habilidad():
    """Objeto que contiene la informacion de una habilidad y su efecto
en el juego"""

    def __init__(self, titulo='', activacion='', costo=(), efectos=(),
                 extra=()):
        self.titulo = titulo
        self.activacion = activacion
        self.costo = costo
        self.efectos = efectos
        self.extra = extra

    def __str__(self):
        lista = []
        for efecto in self.efectos:
            lista.append(''.join([str(f) for f in efecto]))
        return '{}: '.format(self.titulo) + ' - '.join(lista)


class Token():
    """Objeto que representa una cantidad de un tipo de objetos"""

    def __init__(self, tipo='', valor=1):
        self.tipo = tipo
        self.valor = valor

    def __str__(self):
        return '{} {}'.format(self.valor, self.tipo)


class Atributos():
    """Objeto que representa los atributos de una carta tipo Personaje"""

    def __init__(self, resistencia, energia, agilidad):
        self.resistencia = resistencia
        self.energia = energia
        self.agilidad = agilidad

    def __str__(self):
        return 'R{}-E{}-H{}'.format(self.resistencia, self.energia,
                                    self.agilidad)


class Recurso():
    """Objeto que representa el elemento de las cartas tipo Recurso que
    indica los recursos que otorga la carta"""

    def __init__(self, tipo, valor):
        self.tipo = tipo
        self.valor = valor

    def __str__(self):
        return '{} {}'.format(self.valor, self.tipo)


class Carta():
    """Clase base para Cartas"""

    def __init__(self,
                 nombre='',
                 tipo='',
                 nivel=0,
                 clases=(),
                 atributos=None,
                 recurso=None,
                 habilidad=None,
                 imagen=Imagen(),
                 metadatos=Metadatos(),
                 archivo=None):
        self.nombre = nombre
        self.tipo = tipo
        self.nivel = int(nivel)
        self.clases = clases
        self.atributos = atributos
        self.recurso = recurso
        self.habilidad = habilidad
        self.imagen = imagen
        self.metadatos = metadatos
        self.archivo = archivo
        self._activa = True

    @property
    def activa(self):
        return bool(self._activa)

    @activa.setter
    def activa(self, valor):
        self._activa = bool(valor)

    def __bool__(self):
        return self.activa

    def __hash__(self):
        return hash(self.metadatos)

    def __eq__(self, carta):
        if issubclass(type(carta), Carta):
            return bool(hash(carta) == hash(self))
        else:
            return NotImplemented

    def __ne__(self, carta):
        return not self.__eq__(carta)

    def __str__(self):
        return '{}({}-{})'.format(self.nombre, self.tipo, self.nivel)


class MazoCartas(list):
    """Contenedor que representa un Mazo de Cartas"""

    def __init__(self, tamano=10):
        list.__init__(self)
        self.tamano = tamano
        self.extend([None for i in range(tamano)])

    def tomar(self, cantidad=1):
        """Tomar Cartas del mazo"""
        self.extend([None for i in range(cantidad)])
        cartas = [self.pop(0) for i in range(cantidad)]
        return cartas

    def agregar(self, cartas):
        """Agregar Cartas al mazo"""
        for carta in cartas:
            carta.activa = True
        for carta in range(self.tamano-1, -1, -1):
            if self[carta] is None:
                del self[carta]
        if len(cartas) <= (self.tamano - len(self)):
            self.extend(cartas)
            self.extend([None for i in range(self.tamano-len(self))])
        else:
            raise Exception('El mazo esta lleno')

    def __len__(self):
        numero = 0
        for carta in self:
            if carta is not None:
                numero += 1
        return numero


def crear_carta_xml(archivo):
    """Crea un objeto Carta desde un archivo XML"""
    xml = parse(archivo).getroot()
    imagen = None
    metadatos = None
    habilidad = None
    atributos = None
    recurso = None
    for nodo in xml:
        if nodo.tag == 'imagen':
            imagen = crear_imagen_xml(nodo)
        elif nodo.tag == 'metadatos':
            metadatos = crear_metadatos_xml(nodo)
        elif nodo.tag == 'habilidad':
            habilidad = crear_habilidad_xml(nodo)
        elif nodo.tag == 'atributos':
            atributos = crear_atributos_xml(nodo)
        elif nodo.tag == 'recurso':
            recurso = crear_recurso_xml(nodo)
    return Carta(
        nombre=xml[0].text,
        tipo=xml.get('tipo'),
        nivel=xml[1].text,
        clases=[clase.text for clase in xml[2]],
        atributos=atributos,
        recurso=recurso,
        habilidad=habilidad,
        imagen=imagen,
        metadatos=metadatos,
        archivo=archivo,
    )


def crear_imagen_xml(nodo):
    """Crea un objeto Imagen desde un nodo XML"""
    return Imagen(
        archivo=nodo.get('archivo'),
        historia=nodo[0].text,
        artista=nodo[1].text)


def crear_atributos_xml(nodo):
    """Crea un objeto Imagen desde un nodo XML"""
    return Atributos(
        resistencia=nodo[0].text,
        energia=nodo[1].text,
        agilidad=nodo[2].text)


def crear_recurso_xml(nodo):
    """Crea un objeto recurso desde un nodo XML"""
    return Recurso(
        tipo=nodo.get('tipo'),
        valor=nodo.get('valor'))


def crear_metadatos_xml(nodo):
    """Crea un objeto Metadatos desde un nodo XML"""
    return Metadatos(temporada=nodo[0].get('numero'),
                     expancion=nodo[1].get('numero'),
                     serial=nodo[2].text,
                     vercion=nodo[3].text,
                     nombre_temporada=nodo[0].text,
                     nombre_expancion=nodo[1].text)


def crear_habilidad_xml(nodo):
    """Crea un objeto Efecto desde un nodo XML"""
    titulo = nodo[0].text
    activacion = nodo[1].text
    costo = [Token(token.get('tipo'), token.get('valor'))
             for token in nodo[2]]
    extra = [Token(token.get('tipo'), token.get('valor'))
             for token in nodo[-1]]
    # Obtener efectos
    efectos = []
    for subnodo in (subnodo for subnodo in nodo if subnodo.tag == 'efecto'):
        efecto = []
        efecto.append(subnodo.text)
        for token in subnodo:
            efecto.append(Token(token.get('tipo'), token.get('valor')))
            efecto.append(token.tail)
        efectos.append(list(efecto))
    return Habilidad(titulo=titulo, activacion=activacion,
                     costo=costo, efectos=efectos, extra=extra)


def buscar_carta(codigo):
    """Función de ayuda para encontrar un objeto Carta de la lista local"""
    for carta in CARTAS:
        if hash(carta) == codigo:
            return copy(carta)
    raise Exception('Carta no encontrada')

CARTAS = [crear_carta_xml(carta) for carta in LISTA_CARTAS]
REG.debug('Base local de cartas generada')
