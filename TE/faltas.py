"""Exepciones del juego

Indican las diferentes formas en que las reglas pueden ser violadas.
"""


class Falta(Exception):
    """Error base para faltas de las reglas del juego"""

    formato = '{}'

    def __init__(self, *args):
        Exception.__init__(*args)
        mensaje = self.formato.format(*args)
        self.args = (mensaje,)


class LimiteReacurso(Falta):
    """Los personajes no pueden equiparse mas recuros de un tipo que su
limite"""

    formato = 'El personaje a alcansado el limite de {} equipable'


class NoSuficientesRecursos(Falta):
    """Los personajes no pueden desactivar o destruir mas recursos de los que
tienen"""

    formato = 'El personaje no tiene {} {}'


class PersonajeOcupado(Falta):
    """Personaje esta ocupado"""
    pass


class NoSuficienteRango(Falta):
    """La distancia es mayor que el Rango permitido"""
    pass


class NoSuficienteNivel(Falta):
    """Los personajes no pudeden equiparse recursos con un nivel mayor al
suyo"""

    formato = 'Un Personaje nivel {} no puede equiparse un Recurso nivel {}'
