#!/usr/bin/env python
# -*- coding: utf-8 -*-

from TE.globales import *
from TE.faltas import *

from TE.mapa import Pos

"""Efecto empujar 1"""

variables = {'objetivo'}
duracion = PERIODO

costos = {'Agilidad':1}
extras = {'Rango':1}

def efecto(jugador, personaje, reglas, valores):
    inicio =  reglas.juego.mapa.buscar(personaje)
    final = valores['objetivo']
    var = Pos(1*(final.x-inicio.x), 1*(final.y-inicio.y))
    pos = final + var
    objetivo = reglas.juego.mapa[valores['objetivo']]
    personaje = reglas.juego.buscarPersonaje(objetivo)

    fondo = reglas.juego.mapa.obtenerFondo(pos)
    frente = reglas.juego.mapa.obtenerPersonaje(pos)

    if fondo and fondo.caminable:
        if not frente or not frente.bloqueo:
            reglas.juego.mapa.mover(personaje, pos)

    elif not fondo or not fondo.caminable:
        reglas.juego.mapa.quitar(final)
        objetivo.activo = False
